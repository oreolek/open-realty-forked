<div class="<?php echo $divClass; ?>">
    <span class="search"><div class="<?php echo $textClass; ?>"><?php echo Yii::t('common', 'Property type'); ?>:</div> </span>
    <span class="search">
    <?php
    echo CHtml::dropDownList(
        'objType',
        isset($this->objType) ? $this->objType : 0, CMap::mergeArray(array(0 => Yii::t('common', 'Anything')),
        Apartment::getObjTypesArray()),
        array('class' => $fieldClass, 'multiple' => TRUE)
    );
    Yii::app()->clientScript->registerScript('objType', '
		focusSubmit($("select#objType"));
	', CClientScript::POS_READY);
    ?>
    </span>
</div>
