<?php
/**********************************************************************************************
 *                            CMS Open Real Estate
 *                              -----------------
 *	version				:	1.9.1
 *	copyright			:	(c) 2014 Monoray
 *	website				:	http://www.monoray.ru/
 *	contact us			:	http://www.monoray.ru/contact
 *
 * This file is part of CMS Open Real Estate
 *
 * Open Real Estate is free software. This work is licensed under a GNU GPL.
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * Open Real Estate is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * Without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 ***********************************************************************************************/

class HUser {
	const UPLOAD_MAIN = 'main';
	const UPLOAD_PORTFOLIO = 'portfolio';
	const UPLOAD_AVA = 'ava';

	private static $_model;

	public static function getUploadDirectory(User $user, $category = self::UPLOAD_MAIN) {
		$DS = DIRECTORY_SEPARATOR;
		$root = ROOT_PATH . $DS . 'uploads' . $DS . $category;
		self::genDir($root);

		$year = date('Y', strtotime($user->date_created));
		$path = $root . $DS . $year;
		self::genDir($path);

		$month = date('m', strtotime($user->date_created));
		$path = $path . $DS . $month;
		self::genDir($path);

		return $path;
	}

	public static function getUploadUrl(User $user, $category = self::UPLOAD_MAIN){
		$DS = '/';
		$root = 'uploads' . $DS . $category;

		$year = date('Y', strtotime($user->date_created));
		$path = $root . $DS . $year;

		$month = date('m', strtotime($user->date_created));
		$path = $path . $DS . $month;

		return Yii::app()->baseUrl . $DS . $path;
	}

	public static function genDir($path){
		if(!is_dir($path)){
			if(!mkdir($path)){
				throw new CException('HUser невозможно создать директорию ' . $path);
			}
		}
	}

	public static function getModel()
	{
		if(!isset(self::$_model)){
			self::$_model = User::model()->findByPk(Yii::app()->user->id);
		}

		return self::$_model;
	}

	public static function getListAgency(){
		$sql = "SELECT id, agency_name FROM {{users}} WHERE active = 1 AND type=:type";
		$all = Yii::app()->db->createCommand($sql)->queryAll(true, array(':type' => User::TYPE_AGENCY));
		$list = CHtml::listData($all, 'id', 'agency_name');

		return CMap::mergeArray(array(0 => ''), $list);
	}

	public static function getLinkDelAgent(User $user){
		return CHtml::link(tc('Delete'), Yii::app()->createUrl('/usercpanel/main/deleteAgent', array('id' => $user->id)));
	}

	public static function returnStatusHtml($data, $tableId){
		$statuses = User::getAgentStatusList();

		$options = array(
			'onclick' => 'ajaxSetAgentStatus(this, "'.$tableId.'", "'.$data->id.'"); return false;',
		);

		return '<div align="center" class="editable_select" id="editable_select-'.$data->id.'">'.CHtml::link($statuses[$data->agent_status], '#' , $options).'</div>';
	}

	public static function getCountAwaitingAgent($agencyUserID){
		$sql = "SELECT COUNT(id) FROM {{users}} WHERE agency_user_id = :user_id AND agent_status = :status AND active = 1";
		return Yii::app()->db->createCommand($sql)->queryScalar(array(
			':user_id' => $agencyUserID,
			':status' => User::AGENT_STATUS_AWAIT_VERIFY,
		));
	}

	public static function getMenu(){
		$user = HUser::getModel();

		if(param('useUserads')){
			$menu[] = array(
				'label' => tc('My listings'),
				'url' => Yii::app()->createUrl('/usercpanel/main/index'),
				'active' => Yii::app()->controller->menuIsActive('my_listings'),
			);

			$menu[] = array(
				'label' => tc('Add ad', 'apartments'),
				'url' => Yii::app()->createUrl('/userads/main/create'),
				'active' => Yii::app()->controller->menuIsActive('add_ad'),
			);
		}

		if($user->type == User::TYPE_AGENCY){
			$countAwaitAgent = HUser::getCountAwaitingAgent($user->id);
			$bage = $countAwaitAgent ? ' (' .$countAwaitAgent. ')' : '';

			$menu[] = array(
				'label' => tt('My agents', 'usercpanel').$bage,
				'url' => Yii::app()->createUrl('/usercpanel/main/agents'),
				'active' => Yii::app()->controller->menuIsActive('my_agents'),
			);
		}

		$menu[] = array(
			'label' => tc('My data'),
			'url' => Yii::app()->createUrl('/usercpanel/main/data'),
			'active' => Yii::app()->controller->menuIsActive('my_data'),
		);
		$menu[] = array(
			'label' => tt('Change your password', 'usercpanel'),
			'url' => Yii::app()->createUrl('/usercpanel/main/changepassword'),
			'active' => Yii::app()->controller->menuIsActive('my_changepassword'),
		);

		if (issetModule('payment')) {
			$menu[] = array(
				'label' => tt('My payments', 'usercpanel'),
				'url' => Yii::app()->createUrl('/usercpanel/main/payments'),
				'active' => Yii::app()->controller->menuIsActive('my_payments'),
			);
			$menu[] = array(
				'label' => tc('My balance') . ' (' . $user->balance . ' ' . Currency::getDefaultCurrencyName() . ')',
				'url' => Yii::app()->createUrl('/usercpanel/main/balance'),
				'active' => Yii::app()->controller->menuIsActive('my_balance'),
			);
		}

		if (issetModule('bookingtable')) {
			$menu[] = array(
				'label' => tt('Booking applications', 'usercpanel')  . ' (' . Bookingtable::getCountNew(true) . ')',
				'url' => Yii::app()->createUrl('/bookingtable/main/index'),
				'active' => Yii::app()->controller->menuIsActive('booking_applications'),
			);
		}

    $menu[] = array(
				'label' => tc('Database'),
				'url' => Yii::app()->createUrl('/orders/backend/main/admin'),
		);

		return $menu;
	}

  public static function getAdminMenu(){
    $baseUrl = Yii::app()->request->baseUrl;
   	$bagePayments = '';
  	if(issetModule('payment')){
	  	$countPaymentWait = Payments::getCountWait();
  		$bagePayments = ($countPaymentWait > 0) ? "&nbsp<span class=\"badge\">{$countPaymentWait}</span>" : '';
	  }

  	$bageComments = '';
	  if(issetModule('comments')){
  		$countCommentPending = Comment::getCountPending();
	  	$bageComments = ($countCommentPending > 0) ? "&nbsp<span class=\"badge\">{$countCommentPending}</span>" : '';
  	}

	  $bageComplain = '';
  	if(issetModule('apartmentsComplain')){
	  	$countComplainPending = ApartmentsComplain::getCountPending();
  		$bageComplain = ($countComplainPending > 0) ? "&nbsp<span class=\"badge\">{$countComplainPending}</span>" : '';
	  }

  	$bageReviews = '';
	  if(issetModule('reviews')){
			$countReviewsPending = Reviews::getCountModeration();
			$bageReviews = ($countReviewsPending > 0) ? "&nbsp<span class=\"badge\">{$countReviewsPending}</span>" : '';
	  }

	  $countApartmentModeration = Apartment::getCountModeration();
	  $bageListings = ($countApartmentModeration > 0) ? "&nbsp<span class=\"badge\">{$countApartmentModeration}</span>" : '';
    $menu = array(
      //array('label' => tc('Listings')),
      array('label' => tc('Listings') . $bageListings, 'icon' => 'icon-list-alt', 'url' => $baseUrl . '/apartments/backend/main/admin', 'active' => isActive('apartments'), 'urlAdd' => $baseUrl . '/apartments/backend/main/create'),
      array('label' => tc('Orders'), 'icon' => 'icon-list-alt', 'url' => $baseUrl . '/orders/backend/main/admin', 'active' => isActive('orders'), 'urlAdd' => $baseUrl . '/orders/backend/main/create'),
      //array('label' => tc('Comments') . $bageComments, 'icon' => 'icon-list-alt', 'url' => $baseUrl . '/comments/backend/main/admin', 'active' => isActive('comments')),
      array('label' => tc('Complains') . $bageComplain, 'icon' => 'icon-list-alt', 'url' => $baseUrl . '/apartmentsComplain/backend/main/admin', 'active' => isActive('apartmentsComplain'), 'visible' => issetModule('apartmentsComplain')),

      //array('label' => tt('Leads', 'leads')),
      array('label' => tc('Leads'), 'icon' => 'icon-list-alt', 'url' => $baseUrl . '/leads/backend/main/admin', 'active' => isActive('leads'), 'urlAdd' => $baseUrl . '/leads/backend/main/create'),

      //array('label' => tt('Reviews', 'reviews')),
      array('label' => tt('Reviews_management', 'reviews') . $bageReviews, 'icon' => 'icon-list-alt', 'url' => $baseUrl . '/reviews/backend/main/admin', 'active' => isActive('reviews'), 'urlAdd' => $baseUrl . '/reviews/backend/main/create'),
    );
    if (Yii::app()->user->getState('isAdmin'))
    {
    $menu = array_merge($menu, array(
      //array('label' => tc('Users')),
      array('label' => tc('Users'), 'icon' => 'icon-list-alt', 'url' => $baseUrl . '/users/backend/main/admin', 'active' => isActive('users'), 'urlAdd' => $baseUrl . '/users/backend/main/create'),

      array('label' => tc('Content')),
      array('label' => tc('News'), 'icon' => 'icon-file', 'url' => $baseUrl . '/news/backend/main/admin', 'active' => isActive('news')),
      array('label' => tc('Q&As'), 'icon' => 'icon-file', 'url' => $baseUrl . '/articles/backend/main/admin', 'active' => isActive('articles')),
      array('label' => tc('Top menu items'), 'icon' => 'icon-file', 'url' => $baseUrl . '/menumanager/backend/main/admin', 'active' => isActive('menumanager')),
      array('label' => tc('Info pages'), 'icon' => 'icon-file', 'url' => $baseUrl . '/infopages/backend/main/admin', 'active' => isActive('infopages')),

      array('label' => tc('Payments'), 'visible' => issetModule('payment')),
      array('label' => tc('Paid services'), 'icon' => 'icon-shopping-cart', 'url' => $baseUrl . '/paidservices/backend/main/admin', 'active' => isActive('paidservices'), 'visible' => issetModule('payment')),
      array('label' => tc('Manage payments') . $bagePayments, 'icon' => 'icon-shopping-cart', 'url' => $baseUrl . '/payment/backend/main/admin', 'active' => isActive('payment'), 'visible' => issetModule('payment')),
      array('label' => tc('Payment systems'), 'icon' => 'icon-wrench', 'url' => $baseUrl . '/payment/backend/paysystem/admin', 'active' => isActive('payment.paysystem'), 'visible' => issetModule('payment')),

      array('label' => tc('References')),
      array('label' => tc('Categories of references'), 'icon' => 'icon-asterisk', 'url' => $baseUrl . '/referencecategories/backend/main/admin', 'active' => isActive('referencecategories')),
      array('label' => tc('Values of references'), 'icon' => 'icon-asterisk', 'url' => $baseUrl . '/referencevalues/backend/main/admin', 'active' => isActive('referencevalues')),
      //array('label' => tc('Reference "View:"'), 'icon' => 'icon-asterisk', 'url' => $baseUrl . '/windowto/backend/main/admin', 'active' => isActive('windowto')),
      //array('label' => tc('Reference "Check-in"'), 'icon' => 'icon-asterisk', 'url' => $baseUrl . '/timesin/backend/main/admin', 'active' => isActive('timesin')),
      //array('label' => tc('Reference "Check-out"'), 'icon' => 'icon-asterisk', 'url' => $baseUrl . '/timesout/backend/main/admin', 'active' => isActive('timesout')),
      array('label' => tc('Reference "Property types"'), 'icon' => 'icon-asterisk', 'url' => $baseUrl . '/apartmentObjType/backend/main/admin', 'active' => isActive('apartmentObjType')),
      array('label' => tc('Reference "City/Cities"'), 'icon' => 'icon-asterisk', 'url' => $baseUrl . '/apartmentCity/backend/main/admin', 'active' => isActive('apartmentCity'), 'visible' => !(issetModule('location') && param('useLocation', 1))),
      array('label' => tc('Reference "District/Districts"'), 'icon' => 'icon-asterisk', 'url' => $baseUrl . '/apartmentDistrict/backend/main/admin', 'active' => isActive('apartmentDistrict'), 'visible' => !(issetModule('location') && param('useLocation', 1))),

      array('label' => tc('Settings')),
      array('label' => tc('Settings'), 'icon' => 'icon-wrench', 'url' => $baseUrl . '/configuration/backend/main/admin', 'active' => isActive('configuration')),
      //array('label' => tc('Manage modules'), 'icon' => 'icon-wrench', 'url' => $baseUrl . '/modules/backend/main/admin', 'active' => isActive('modules')),
      array('label' => tc('Images'), 'icon' => 'icon-wrench', 'url' => $baseUrl . '/images/backend/main/index', 'active' => isActive('images')),
      array('label' => tc('Change admin password'), 'icon' => 'icon-wrench', 'url' => $baseUrl . '/adminpass/backend/main/index', 'active' => isActive('adminpass')),
      array('label' => tc('Seo settings'), 'icon' => 'icon-wrench', 'url' => $baseUrl . '/seo/backend/main/admin', 'active' => isActive('seo'), 'visible' => issetModule('seo')),
      array('label' => 'Переводы', 'icon' => 'icon-file', 'url' => $baseUrl . '/translateMessage/backend/main/admin', 'active' => isActive('translationMessages')),
      /*
      array('label' => tc('Authentication services'), 'icon' => 'icon-wrench', 'url' => $baseUrl . '/socialauth/backend/main/admin', 'active' => isActive('socialauth'), 'visible' => issetModule('socialauth')),

      array('label' => tc('Modules'), 'visible' => (issetModule('notifier') || issetModule('slider') || issetModule('advertising') || issetModule('iecsv') || issetModule('formdesigner') || issetModule('socialposting'))),
      array('label' => tc('Mail editor'), 'icon' => 'icon-circle-arrow-right', 'url' => $baseUrl . '/notifier/backend/main/admin', 'active' => isActive('notifier'), 'visible' => issetModule('notifier')),
      array('label' => tc('Slide-show on the Home page'), 'icon' => 'icon-circle-arrow-right', 'url' => $baseUrl . '/slider/backend/main/admin', 'active' => isActive('slider'), 'visible' => issetModule('slider')),
      array('label' => tc('Import / Export'), 'icon' => 'icon-circle-arrow-right', 'url' => $baseUrl . '/iecsv/backend/main/admin', 'active' => isActive('iecsv'), 'visible' => issetModule('iecsv')),
      array('label' => tc('Advertising banners'), 'icon' => 'icon-circle-arrow-right', 'url' => $baseUrl . '/advertising/backend/advert/admin', 'active' => isActive('advertising'), 'visible' => issetModule('advertising')),
      array('label' => tc('The forms designer'), 'icon' => 'icon-circle-arrow-right', 'url' => $baseUrl . '/formdesigner/backend/main/admin', 'active' => isActive('formdesigner'), 'visible' => issetModule('formdesigner')),
      array('label' => tt('Services of automatic posting', 'socialposting'), 'icon' => 'icon-circle-arrow-right', 'url' => $baseUrl . '/socialposting/backend/main/admin', 'active' => isActive('socialposting'), 'visible' => issetModule('socialposting')),
      */

      array('label' => tc('Location module'), 'visible' => (issetModule('location') && param('useLocation', 1))),
      array('label' => tc('Countries'), 'icon' => 'icon-globe', 'url' => $baseUrl . '/location/backend/country/admin', 'visible' => (issetModule('location') && param('useLocation', 1)), 'active' => isActive('location.country')),
      array('label' => tc('Regions'), 'icon' => 'icon-globe', 'url' => $baseUrl . '/location/backend/region/admin', 'visible' => (issetModule('location') && param('useLocation', 1)), 'active' => isActive('location.region')),
      array('label' => tc('Cities'), 'icon' => 'icon-globe', 'url' => $baseUrl . '/location/backend/city/admin', 'visible' => (issetModule('location') && param('useLocation', 1)), 'active' => isActive('location.city')),
    ));
    }
    return $menu;
  }
}
