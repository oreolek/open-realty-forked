<?php
Yii::import('bootstrap.widgets.TbBaseMenu');
Yii::import('bootstrap.widgets.TbMenu');

/**
 * Меню в админке.
 * @license GPL v2
 * @author Oreolek
 */
class AdminMenu extends TbMenu
{
  /**
	 * Renders the content of a menu item.
	 * Note that the container and the sub-menus are not rendered here.
	 * @param array $item the menu item to be rendered. Please see {@link items} on what data might be in the item.
	 * @return string the rendered item
	 */
	protected function renderMenuItem($item)
	{
    $retval = $item['label'];
		if (isset($item['icon']))
		{
			if (strpos($item['icon'], 'icon') === false)
			{
				$pieces = explode(' ', $item['icon']);
				$item['icon'] = 'icon-'.implode(' icon-', $pieces);
			}

			$item['label'] = '<i class="'.$item['icon'].'"></i> '.$item['label'];
		}

		if (!isset($item['linkOptions']))
			$item['linkOptions'] = array();

		if (isset($item['items']) && !empty($item['items']))
		{
			$item['url'] = '#';

			if (isset($item['linkOptions']['class']))
				$item['linkOptions']['class'] .= ' dropdown-toggle';
			else
				$item['linkOptions']['class'] = 'dropdown-toggle';

			$item['linkOptions']['data-toggle'] = 'dropdown';
			$item['label'] .= ' <span class="caret"></span>';
		}

    if (isset($item['urlAdd']))
    {
      if (isset($item['linkOptions']['class']))
				$item['linkOptions']['class'] .= ' short';
			else
				$item['linkOptions']['class'] = 'short';
    }

		if (isset($item['url'])){
			if(isset($item['linkOptions']['submit'])){
				$item['linkOptions']['csrf'] = true;
			}

			$retval = CHtml::link($item['label'], $item['url'], $item['linkOptions']);
		}

    if (isset($item['urlAdd']))
    {
			$retval .= CHtml::link('<i class="icon-plus-sign"></i>', $item['urlAdd'], array('class' => 'icon'));
    }

    return $retval;
	}

}
