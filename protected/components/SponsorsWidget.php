<?php
/**********************************************************************************************
* @author Alexander Yakovlev
* This work is licensed under a GNU GPL.
* http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
***********************************************************************************************/

class SponsorsWidget extends CWidget {
	public function run() {
    echo '<h3>Наши партнёры</h3>';
    // ВТБ24, Сбербанк, АИЖК, Банк Москвы, Уралсиб, Газпромбанк
    // ширина 235
    $array = explode(',', param('partnerImages'));//.png
    foreach ($array as $img)
    {
      echo '<img src="/images/sponsors/'.trim($img).'.png">';
    }
	}
}
