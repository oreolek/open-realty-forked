<?php

class m150106_024602_orderUpdate extends CDbMigration
{
	public function safeUp()
	{
    $this->dropColumn('{{orders}}', 'square_max');
    $this->dropColumn('{{orders}}', 'square_min');
    $this->dropColumn('{{orders}}', 'is_active');
    $this->addColumn('{{orders}}', 'square_min', 'float');
    $this->addColumn('{{orders}}', 'square_max', 'float');
	}

	public function safeDown()
	{
    $this->dropColumn('{{orders}}', 'square_max');
    $this->dropColumn('{{orders}}', 'square_min');
    $this->addColumn('{{orders}}', 'is_active', 'boolean');
    $this->addColumn('{{orders}}', 'square_min', 'integer');
    $this->addColumn('{{orders}}', 'square_max', 'integer');
	}
}
