<?php

class m150213_091853_leads extends CDbMigration
{
	public function safeUp()
	{
    $this->addColumn('{{apartment}}', 'lead_id', 'integer');
	}

	public function safeDown()
	{
    $this->dropColumn('{{apartment}}', 'lead_id');
	}
}
