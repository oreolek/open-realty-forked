<?php

class m141229_090824_defaultCity extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
    $this->delete('{{apartment_city}}');
    $this->insert('{{apartment_city}}', array(
      'id' => 1,
      'name_ru' => 'Кемерово',
      'sorter' => 3,
      'active' => 1,
      'date_updated' => '2014-12-27 16:21:14'
    ));
	}

	public function safeDown()
	{
    $this->delete('{{apartment_city}}', array('id' => 1));
	}
}
