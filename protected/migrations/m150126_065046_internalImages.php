<?php

class m150126_065046_internalImages extends CDbMigration
{
	public function safeUp()
	{
    $this->dropColumn('{{images}}', 'is_public');
    $this->addColumn('{{images}}', 'scope', 'integer');
	}

	public function safeDown()
	{
    $this->addColumn('{{images}}', 'is_public', 'boolean');
    $this->dropColumn('{{images}}', 'scope');
	}
}
