<?php

class m150116_084754_searchForm extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
    $this->delete('{{search_form}}', 'field = "by_id"');
    $this->delete('{{search_form}}', 'field = "ap_type"');
    $this->update('{{search_form}}', array('sorter' => 3), 'field = "floor"');
    $this->update('{{search_form}}', array('sorter' => 8), 'field = "square" AND obj_type_id = 0');
    $this->update('{{search_form}}', array('sorter' => 7), 'field = "rooms" AND obj_type_id = 0');
	}

	public function safeDown()
	{
    $by_id = array(
      'id' => '32',
      'status' => 1,
      'compare_type' => 0,
      'obj_type_id' => 0,
      'sorter' => 10,
      'formdesigner_id' => 0,
      'field' => 'by_id'
    );
    $this->insert('{{search_form}}', $by_id);
    $by_id['id'] = 14; $by_id['obj_type_id'] = '1'; $by_id['sorter'] = '9';
    $this->insert('{{search_form}}', $by_id);
    $by_id['id'] = 25; $by_id['obj_type_id'] = '3'; $by_id['sorter'] = '8';
    $this->insert('{{search_form}}', $by_id);
    $by_id['id'] = 47; $by_id['obj_type_id'] = '4'; $by_id['sorter'] = '7';
    $this->insert('{{search_form}}', $by_id);
    $by_id['id'] = 20; $by_id['obj_type_id'] = '2'; $by_id['sorter'] = '9';
    $this->insert('{{search_form}}', $by_id);
    $this->execute('INSERT INTO `{{search_form}}` VALUES (1,2,0,1,"ap_type", 3, 0);');
    $this->execute('INSERT INTO `{{search_form}}` VALUES (2,2,0,2,"ap_type", 3, 0);');
    $this->execute('INSERT INTO `{{search_form}}` VALUES (3,2,0,3,"ap_type", 3, 0);');
    $this->execute('INSERT INTO `{{search_form}}` VALUES (7,2,0,0,"ap_type", 3, 0);');
    $this->execute('INSERT INTO `{{search_form}}` VALUES (34,2,0,4,"ap_type", 3, 0);');
    $this->update('{{search_form}}', array('sorter' => 6), 'field = "square"');
	}
}
