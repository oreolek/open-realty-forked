<?php

class m141224_055321_InternalComments extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
    if(! isset(Yii::app()->getDb()->tablePrefix)) { Yii::app()->getDb()->tablePrefix = ''; };
    $this->addColumn('{{comments}}', 'is_public', 'boolean');
    $this->addColumn('{{images}}', 'is_public', 'boolean');
    $this->createTable('{{leads}}', array(
      'id' => 'pk',
      'name' => 'string NOT NULL',
      'phone' => 'string',
      'email' => 'string',
      'additional' => 'longtext',
      'user_id' => 'integer NOT NULL'
    ));
    $this->createTable('{{orders}}', array(
      'id' => 'pk',
      'lead_id' => 'integer NOT NULL',
      'apartment_id' => 'integer NOT NULL',
      'is_active' => 'integer NOT NULL',
      'user_id' => 'integer NOT NULL',
      'created_at' => 'datetime',
      'updated_at' => 'datetime',
      'price_min' => 'integer',
      'price_max' => 'integer',
      'district_id' => 'integer NOT NULL',
      'rooms_min' => 'integer',
      'rooms_max' => 'integer',
      'status' => 'integer NOT NULL',
      'square_min' => 'integer',
      'square_max' => 'integer',
      'floor' => 'integer',
      'additional' => 'longtext'
    ));
    $this->createTable('{{districts}}', array(
      'id' => 'pk',
      'city_id' => 'integer NOT NULL',
      'name_ru' => 'string NOT NULL'
      'sorter' => 'integer'
    ));
    $this->addColumn('{apartment}}', 'district_id', 'integer NOT NULL');
    $this->dropColumn('{apartment}}', 'city_id');
    $this->addForeignKey('fk_leads_users', '{{leads}}', 'user_id', '{{users}}', 'id');
    $this->addForeignKey('fk_orders_users', '{{orders}}', 'user_id', '{{users}}', 'id');
    $this->addForeignKey('fk_orders_leads', '{{orders}}', 'lead_id', '{{leads}}', 'id');
    $this->addForeignKey('fk_orders_districts', '{{orders}}', 'district_id', '{{districts}}', 'id');
	}

	public function safeDown()
	{
    if(! isset(Yii::app()->getDb()->tablePrefix)) { Yii::app()->getDb()->tablePrefix = ''; };
    $this->addColumn('{apartment}}', 'city_id', 'integer NOT NULL');
    $this->dropColumn('{{comments}}', 'is_public');
    $this->dropColumn('{{images}}', 'is_public');
    $this->dropForeignKey('fk_orders_districts', '{{orders}}');
    $this->dropForeignKey('fk_orders_leads', '{{orders}}');
    $this->dropForeignKey('fk_orders_users', '{{orders}}');
    $this->dropForeignKey('fk_leads_users', '{{leads}}');
    $this->dropTable('{{districts}}');
    $this->dropTable('{{orders}}');
    $this->dropTable('{{leads}}');
    $this->dropColumn('{{apartment}}', 'district_id');
	}
}
