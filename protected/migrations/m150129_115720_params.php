<?php

class m150129_115720_params extends CDbMigration
{
	public function up()
	{
    $this->insert('{{configuration}}', array(
      'type' => 'text',
      'section' => 'main',
      'name' => 'logoImage',
      'value' => '/images/maxinvest.png',
      'allowEmpty' => 0
    ));
    $this->insert('{{configuration}}', array(
      'type' => 'text',
      'section' => 'main',
      'name' => 'partnerImages',
      'value' => 'vtb, sber, gazprom, AHML, uralsib, moscow',
      'allowEmpty' => 1
    ));
	}

	public function down()
	{
		echo "m150129_115720_params does not support migration down.\n";
		return false;
	}
}
