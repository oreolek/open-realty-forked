<?php

class m150103_085403_Leads extends CDbMigration
{
	public function safeUp()
	{
    $this->addColumn('{{leads}}', 'sorter', 'integer');
    $this->addColumn('{{orders}}', 'sorter', 'integer');
	}

	public function safeDown()
	{
    $this->dropColumn('{{leads}}', 'sorter');
    $this->dropColumn('{{orders}}', 'sorter');
	}
}
