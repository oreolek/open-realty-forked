<?php

class m141227_092143_translations extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
    $this->insert('{{translate_message}}', array(
      'category' => 'common',
      'status' => 0,
      'message' => 'Reference "District/Districts"',
      'translation_ru' => 'Справочник "Район"',
      'date_updated' => '2014-12-27 16:21:14'
    ));
    $this->insert('{{translate_message}}', array(
      'category' => 'module_apartmentDistrict',
      'status' => 0,
      'message' => 'Manage apartment district',
      'translation_ru' => 'Управление районами городов',
      'date_updated' => '2014-12-27 16:21:14'
    ));
    $this->insert('{{translate_message}}', array(
      'category' => 'module_apartmentDistrict',
      'status' => 0,
      'message' => 'Add district',
      'translation_ru' => 'Добавить район',
      'date_updated' => '2014-12-27 16:21:14'
    ));
	}

	public function safeDown()
	{
    $this->delete('{{translate_message}}', array('category' => 'module_apartmentDistrict'));
    $this->delete('{{translate_message}}', array('message' => 'Reference "District/Districts"'));
	}
}
