<?php
return array(
  'class' => 'CDbConnection',
  'connectionString' => 'mysql:host=localhost;dbname=ore;port=3306',
  'username' => 'root',
  'password' => '',
  'emulatePrepare' => true,
  'charset' => 'utf8',
  'enableParamLogging' => false,
  'enableProfiling' => TRUE,
  'schemaCachingDuration' => 7200,
  'tablePrefix' => 'ore_',
);
