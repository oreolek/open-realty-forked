<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
Yii::getLogger()->autoDump=true; // realtime file logging
Yii::getLogger()->autoFlush=1;

$basePath = dirname(__FILE__).DIRECTORY_SEPARATOR.'..';

require_once(dirname(__FILE__) . '/../helpers/common.php');
require_once(dirname(__FILE__) . '/../helpers/strings.php');
require_once(dirname(__FILE__) . '/../helpers/HUser.php');
require_once(dirname(__FILE__) . '/../helpers/HDate.php');

return array(
	'basePath'=>$basePath,
	'name' => 'MaxInvest',
	'language'=>'ru',
	'sourceLanguage' => 'en',
	'homeurl'=> '/',
	'preload' => array(
		'log',
		'configuration', // preload configuration
	),
	// autoloading model and component classes
	'import' => array(
		'ext.eoauth.*',
		'ext.eoauth.lib.*',
		'ext.lightopenid.*',
		'ext.eauth.*',
		'ext.eauth.services.*',
		'ext.eauth.custom_services.CustomGoogleService',
		'ext.eauth.custom_services.CustomVKService',
		'ext.eauth.custom_services.CustomFBService',
		'ext.eauth.custom_services.CustomTwitterService',
		'ext.eauth.custom_services.CustomMailruService',
		'ext.setReturnUrl.ESetReturnUrlFilter',

		'application.models.*',
		'application.components.*',

		'application.modules.configuration.components.*',
		'application.modules.notifier.components.Notifier',
		'application.modules.booking.models.*',

		'application.modules.comments.models.Comment',
		'application.modules.comments.models.CommentForm',
		'application.modules.windowto.models.WindowTo',
		'application.modules.apartments.models.*',
		'application.modules.news.models.*',
		'application.extensions.image.Image',
		'application.modules.selecttoslider.models.SelectToSlider',
		'application.modules.similarads.models.SimilarAds',
		'application.modules.menumanager.models.Menu',
		'application.modules.windowto.models.WindowTo',
		'application.modules.apartments.components.*',
		'application.modules.apartmentCity.models.ApartmentCity',
		'application.modules.apartmentObjType.models.ApartmentObjType',
		'application.modules.translateMessage.models.TranslateMessage',

		'application.components.behaviors.ERememberFiltersBehavior',
		'application.modules.service.models.Service',

		'application.modules.socialauth.models.SocialauthModel',
		'application.modules.antispam.components.MathCCaptchaAction',

		'application.modules.images.models.*',
		'application.modules.images.components.*',

		'application.modules.lang.models.Lang',

		'zii.behaviors.CTimestampBehavior',
		'application.modules.apartmentsComplain.models.ApartmentsComplain',
		'application.modules.apartmentsComplain.models.ApartmentsComplainReason',
		'application.modules.comparisonList.models.ComparisonList',
		'application.modules.articles.models.Article',
		'application.modules.infopages.models.InfoPages',
		'application.modules.reviews.models.Reviews',
		'application.modules.bookingtable.models.Bookingtable',
		'application.modules.themes.models.Themes',
		'application.components.oldbrowsers.CheckBrowser',
	),

	'modules' => array(
		'news',
		'referencecategories',
		'referencevalues',
		'apartments',
		'apartmentObjType',
		'apartmentCity',
		'comments',
		'booking',
		'windowto',
		'contactform',
		'articles',
		'usercpanel',
		'users',
		'quicksearch',
		'configuration',
		'timesin',
		'timesout',
		'adminpass',
		'specialoffers',
		'install',
		'selecttoslider',
		'similarads',
		'menumanager',
		'userads',
		'translateMessage',
		'service',
		'socialauth',
		'antispam',
		'rss',
		'images',
		'apartmentsComplain',
		'formdesigner',
		'comparisonList',
		'guestad',
		'reviews',
		'bookingtable',
		'modules',
		'infopages',
		'themes',
		'notifier',
	),

	'components' => array(
		'loid' => array(
			'class' => 'application.extensions.lightopenid.loid',
		),
		'eauth' => array(
			// yii-eauth-1.1.8
			'class' => 'ext.eauth.EAuth',
			'popup' => true, // Use popup windows instead of redirect to site of provider
		),

		'user' => array(
			// enable cookie-based authentication
			'allowAutoLogin' => true,
		),

		'configuration' => array(
			'class' => 'Configuration',
			'cachingTime' => 0, // caching configuration for 180 days
		),

		'cache' => array(
			//'class' => 'system.caching.CFileCache',
			'class' => 'system.caching.CApcCache',
			/*'class'=>'system.caching.CMemCache',
			'useMemcached' => true,
			'servers'=>array(
				array('host'=>'127.0.0.1', 'port'=>11211),
			),*/
		),

		'request'=>array(
			'class' => 'application.components.CustomHttpRequest',
			'enableCsrfValidation'=>true,
		),

		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName' => false,
			'class'=>'application.components.CustomUrlManager',
		),

		'mailer' => array(
			'class' => 'application.extensions.mailer.EMailer',
		),

		'db'=>require(dirname(__FILE__) . '/db.php'),

		'errorHandler' => array(
			'errorAction' => 'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
					'ipFilters'=>array('127.0.0.1'),
				),
			),
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages

				//array(
				//	'class'=>'CWebLogRoute',
				//),
			),
		),
		'messages' => array(
			'class' => 'DbMessageSource',
			'forceTranslation' => true,
			'onMissingTranslation' => array('CustomEventHandler', 'handleMissingTranslation'),
		),

		'messagesInFile' => array(
			'class' => 'CPhpMessageSource',
			'forceTranslation' => true,
		),

		'bootstrap'=>array(
			'class'=>'bootstrap.components.Bootstrap', // assuming you extracted bootstrap under extensions
		),
	),

	'params' => array(
		'module_rss_itemsPerFeed' => 20,
		'allowedImgExtensions' => array('jpg', 'jpeg', 'gif', 'png'),
		'maxImgFileSize' => 8 * 1024 * 1024, // maximum file size in bytes
		'minImgFileSize' => 5 * 1024, // min file size in bytes
		'langToInstall' => 'ru',
		'countListingsInComparisonList' => 6, # максимум объявлений в списке сравнения
		'searchMaxField' => 15, // максимальное кол-во полей в поиске,
		'useMinify' => true,
		'useLangPrefixIfOneLang' => 0, // использовать префикс языка в url если активен только 1 язык
	),

);
