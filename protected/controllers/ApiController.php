<?php
class ApiController extends Controller {
  public $layout = FALSE;
	public function init() {
    parent::init();
    header('Content-type: application/json');
  }

  /**
   * District list
   * @param $id integer city ID
   **/
  public function actionDistricts()
  {
    $id = Yii::app()->request->getParam('id');
    $city = ApartmentCity::model()->findByPK($id);
    if (!$city)
    {
      throw new CHttpException(404);
    }
    $districts = CHtml::listData($city->districts, 'id', 'name_'.Yii::app()->language);
    echo CJSON::encode($districts);
  }
}
