<?php

// change the following paths if necessary
$yiic=dirname(__FILE__).'/../../../framework/yiic.php';
$config=dirname(__FILE__).'/config/console.php';

define('ROOT_PATH', dirname(__FILE__));
define('IS_FREE', TRUE);
define('ALREADY_INSTALL_FILE', ROOT_PATH . DIRECTORY_SEPARATOR . 'protected' . DIRECTORY_SEPARATOR
                                . 'runtime' . DIRECTORY_SEPARATOR . 'already_install');

require_once($yiic);
