<?php
$this->breadcrumbs=array(
	tt('Manage leads')
);

$this->menu=array(
	array('label'=>tt('Add lead'), 'url'=>array('/leads/backend/main/create')),
);

$this->adminTitle = tt('Manage leads');

$this->widget('CustomGridView', array(
	'id'=>'apartment-city-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'afterAjaxUpdate' => 'function(){$("a[rel=\'tooltip\']").tooltip(); $("div.tooltip-arrow").remove(); $("div.tooltip-inner").remove();}',
	'columns'=>array(
		array(
            'class'=>'CCheckBoxColumn',
            'id'=>'itemsSelected',
            'selectableRows' => '2',
            'htmlOptions' => array(
                'class'=>'center',
            ),
        ),
		array(
			'header' => 'Имя',
			'name' => 'name',
			//'filter' => false,
		),
    array(
			'header' => tc('Manager'),
			'value' => '$data->user->username',
		),
    array(
			'header' => tc('Phone'),
      'name' => 'phone',
		),
    array(
			'header' => tc('Email'),
      'name' => 'email',
		),
		array(
			//'class'=>'CButtonColumn',
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{up}{down}{update}{delete}',
			'deleteConfirmation' => tc('Are you sure you want to delete this item?'),
			'htmlOptions' => array('class'=>'infopages_buttons_column'),
			'buttons' => array(
				'up' => array(
					'label' => tc('Move an item up'),
					'imageUrl' => $url = Yii::app()->assetManager->publish(
						Yii::getPathOfAlias('zii.widgets.assets.gridview').'/up.gif'
					),
					'url'=>'Yii::app()->createUrl("/leads/backend/main/move", array("id"=>$data->id, "direction" => "up"))',
					'options' => array('class'=>'infopages_arrow_image_up'),
					'visible' => '$data->sorter > 1',
					'click' => "js: function() { ajaxMoveRequest($(this).attr('href'), 'apartment-city-grid'); return false;}",
				),
				'down' => array(
					'label' => tc('Move an item down'),
					'imageUrl' => $url = Yii::app()->assetManager->publish(
						Yii::getPathOfAlias('zii.widgets.assets.gridview').'/down.gif'
					),
					'url'=>'Yii::app()->createUrl("/leads/backend/main/move", array("id"=>$data->id, "direction" => "down"))',
					'options' => array('class'=>'infopages_arrow_image_down'),
					'visible' => '$data->sorter < "'.$maxSorter.'"',
					'click' => "js: function() { ajaxMoveRequest($(this).attr('href'), 'apartment-city-grid'); return false;}",
				),
			),
            'afterDelete'=>'function(link,success,data){ if(success) $("#statusMsg").html(data); }'
		),
	),
)); ?>

<?php
	$this->renderPartial('//site/admin-select-items', array(
		'url' => '/leads/backend/main/itemsSelected',
		'id' => 'leads-grid',
		'model' => $model,
		'options' => array(
			'delete' => Yii::t('common', 'Delete')
		),
	));
?>
