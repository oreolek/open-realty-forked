<div class="form">

<?php $form=$this->beginWidget('CustomForm', array(
	'id'=>$this->modelName.'-form',
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note"><?php echo Yii::t('common', 'Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary($model); ?>

  <div class="rowold">
    <?php echo $form->labelEx($model, 'name'); ?>
    <?php echo $form->textField($model, 'name'); ?>
    <?php echo $form->error($model, 'name'); ?>
  </div>

<!-- if manager or greater -->
<?php if (Yii::app()->user->getState('isAdmin')) { ?>
  <div class="rowold">
    <label>Менеджер, под которым записать контакт</label>
    <?php $users = CHtml::listData(User::model()->findAll(), 'id', 'username'); ?>
    <?php echo $form->dropDownList($model, 'user_id', $users); ?>
    <?php echo $form->error($model, 'user_id'); ?>
  </div>
<?php } ?>

  <div class="rowold">
    <?php echo $form->labelEx($model, 'phone'); ?>
    <?php echo $form->textField($model, 'phone'); ?>
    <?php echo $form->error($model, 'phone'); ?>
  </div>

  <div class="rowold">
    <?php echo $form->labelEx($model, 'email'); ?>
    <?php echo $form->emailField($model, 'email'); ?>
    <?php echo $form->error($model, 'email'); ?>
  </div>

  <div class="rowold">
    <?php echo $form->labelEx($model, 'additional'); ?>
    <?php echo $form->textArea($model, 'additional'); ?>
    <?php echo $form->error($model, 'additional'); ?>
  </div>

	<div class="clear"></div>

    <div class="rowold buttons">
           <?php $this->widget('bootstrap.widgets.TbButton',
                       array('buttonType'=>'submit',
                           'type'=>'primary',
                           'icon'=>'ok white',
                           'label'=> $model->isNewRecord ? tc('Add') : tc('Save'),
                       )); ?>
   	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
