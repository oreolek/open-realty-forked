<?php
$this->breadcrumbs=array(
	tt('Manage leads') => array('admin'),
	tt('Add lead'),
);

$this->menu=array(
	array('label'=>tt('Manage leads'), 'url'=>array('admin')),
);

$this->adminTitle = tt('Add lead');
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
