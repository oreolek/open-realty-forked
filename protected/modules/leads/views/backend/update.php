<?php
$this->breadcrumbs=array(
	tt('Manage leads')=>array('admin'),
	tt('Edit lead'),
);

$this->menu=array(
    array('label'=>tt('Manage leads'), 'url'=>array('admin')),
    array('label'=>tt('Add lead'), 'url'=>array('/leads/backend/main/create')),
);

$this->adminTitle = tt('Edit lead');
?>

<div class="row">
  <div class="span9">
    <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
  </div>
  <div class="span3">
    <div id="comments">
    <?php
		$this->widget('application.modules.comments.components.commentListWidget', array(
			'model' => $model,
			'url' => Yii::app()->request->getUrl(),
		));
  	?>
    </div>
  </div>
</div>
