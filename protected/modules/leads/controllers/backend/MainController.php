<?php
/**********************************************************************************************
* @author Alexander Yakovlev
* This work is licensed under a GNU GPL.
* http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
***********************************************************************************************/

class MainController extends ModuleAdminController{
	public $modelName = 'Lead';
  public $allowAll = TRUE;

	public function actionView($id){
		$this->redirect(array('admin'));
	}
	public function actionIndex(){
		$this->redirect(array('admin'));
	}

  public function actionCreate()
  {
    $model=new $this->modelName;
		if($this->scenario){
			$model->scenario = $this->scenario;
		}
    $model->user_id = Yii::app()->user->id;
		$this->performAjaxValidation($model);

		if(isset($_POST[$this->modelName])){
			$model->attributes=$_POST[$this->modelName];
      if (!Yii::app()->user->getState('isAdmin'))
      {
        $model->user_id = Yii::app()->user->id;
      }
			if($model->save()){
				if (!empty($this->redirectTo))
					$this->redirect($this->redirectTo);
				else
					$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array_merge(
				array('model'=>$model),
				$this->params
		));
  }

	public function actionAdmin(){
		$this->getMaxSorter();
		parent::actionAdmin();
	}
}
