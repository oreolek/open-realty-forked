<?php

/**
 * This is the model class for table "{{leads}}".
 *
 * The followings are the available columns in table '{{leads}}':
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $additional
 * @property integer $user_id
 * @property integer $sorter
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Order[] $orders
 */
class Lead extends ParentModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{leads}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, user_id', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('name, phone, email', 'length', 'max'=>255),
			array('email', 'email'),
			array('additional', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, phone, email, additional, user_id', 'safe', 'on'=>'search'),
		);
	}

  public function i18nFields(){
		return array(
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'orders' => array(self::HAS_MANY, 'Order', 'lead_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Имя, фамилия',
			'phone' => 'Телефон',
			'email' => 'E-mail',
			'additional' => 'Дополнительно',
			'user_id' => 'Менеджер',
		);
	}

  public function beforeDelete()
  {
    if (!Yii::app()->user->getState('isAdmin')) // only admin can delete this
    {
      Yii::app()->user->setFlash('error', 'Только администратор может удалять контакты.');
      return FALSE;
    }
		return parent::beforeDelete();
  }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('additional',$this->additional,true);
		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Lead the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
