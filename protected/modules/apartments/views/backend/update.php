<?php
$this->breadcrumbs=array(
	tt('Manage apartments') => array('admin'),
	tt('Update apartment'),
);

$this->menu = array(
	array('label'=>tt('Manage apartments'), 'url'=>array('admin')),
	array('label'=>tt('Add apartment'), 'url'=>array('create')),
	array('label'=>tt('Delete apartment'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>tc('Are you sure you want to delete this item?'))),
);

$this->adminTitle = tt('Update apartment');
?>

<div class="row">
  <div class="span9">
    <?php echo $this->renderPartial('_form', array(
      'model'=>$model,
			'supportvideoext' => $supportvideoext,
			'supportvideomaxsize' => $supportvideomaxsize,
    )); ?>
  </div>
  <div class="span3">
    <div id="comments">
    <?php
		$this->widget('application.modules.comments.components.commentListWidget', array(
			'model' => $model,
      'order' => 'desc',
      'is_public' => FALSE,
			'url' => Yii::app()->request->getUrl(),
		));
  	?>
    </div>
  </div>
</div>
