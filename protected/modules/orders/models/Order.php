<?php

/**
 * This is the model class for table "{{orders}}".
 *
 * The followings are the available columns in table '{{orders}}':
 * @property integer $id
 * @property integer $lead_id
 * @property integer $apartment_id
 * @property integer $user_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $price_min
 * @property integer $price_max
 * @property integer $district_id
 * @property integer $rooms_min
 * @property integer $rooms_max
 * @property integer $status
 * @property integer $square_min
 * @property integer $square_max
 * @property integer $floor
 * @property string $additional
 *
 * The followings are the available model relations:
 * @property Leads $lead
 * @property Users $user
 */
class Order extends ParentModel
{
  const STATUS_INACTIVE = 0;
  const STATUS_ACTIVE = 1;
  const STATUS_DONE = 2;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{orders}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lead_id, user_id, district_id, status', 'required'),
			array('lead_id, apartment_id, user_id, price_min, price_max, district_id, rooms_min, rooms_max, status, square_min, square_max, floor', 'numerical', 'integerOnly'=>true),
			array('created_at, updated_at, additional', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, lead_id, apartment_id, user_id, created_at, updated_at, price_min, price_max, district_id, rooms_min, rooms_max, status, square_min, square_max, floor, additional', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lead' => array(self::BELONGS_TO, 'Lead', 'lead_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'apartment' => array(self::BELONGS_TO, 'Apartment', 'apartment_id'),
		);
	}

  public function beforeDelete()
  {
    if (!Yii::app()->user->getState('isAdmin')) // only admin can delete this
    {
      Yii::app()->user->setFlash('error', 'Только администратор может удалять заявки.');
      return FALSE;
    }
		return parent::beforeDelete();
  }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'lead_id' => tt('Lead'),
			'apartment_id' => tt('Apartment'),
			'user_id' => tt('User'),
			'created_at' => tt('Created At'),
			'updated_at' => tt('Updated At'),
			'price' => tt('Price'),
			'district_id' => tt('District'),
			'rooms' => tt('Rooms'),
			'square' => tt('Square'),
			'floor' => tt('Floor'),
			'additional' => tt('Additional'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('lead_id',$this->lead_id);
		$criteria->compare('apartment_id',$this->apartment_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('price_min',$this->price_min);
		$criteria->compare('price_max',$this->price_max);
		$criteria->compare('district_id',$this->district_id);
		$criteria->compare('rooms_min',$this->rooms_min);
		$criteria->compare('rooms_max',$this->rooms_max);
		$criteria->compare('status',$this->status);
		$criteria->compare('square_min',$this->square_min);
		$criteria->compare('square_max',$this->square_max);
		$criteria->compare('floor',$this->floor);
		$criteria->compare('additional',$this->additional,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

  public static function statusStateArray()
  {
    return array(
      Order::STATUS_INACTIVE => tc('Inactive'),
      Order::STATUS_ACTIVE => tc('Active'),
      Order::STATUS_DONE => tc('Done'),
    );
  }

  public static function getOccupiedApartmentIds()
  {
    $command = Yii::app()->db->createCommand();
    $command->select('apartment_id');
    $command->setDistinct(TRUE);
    $command->from('{{orders}}');
    return $command->queryColumn();
  }

  protected function beforeValidate()
  {
    if (!Yii::app()->user->getState('isAdmin'))
    {
      $this->user_id = Yii::app()->user->id;
    }
    return TRUE;
  }
}
