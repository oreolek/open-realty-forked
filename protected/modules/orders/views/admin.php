<?php
Yii::app()->clientScript->registerScript('editable_select', "
		function ajaxSetModerationStatus(elem, id, id_elem, owner_id, items){
			$('#editable_select-'+id_elem).editable('".Yii::app()->controller->createUrl("activate")."', {
				data   : items,
				type   : 'select',
				cancel : '".tc('Cancel')."',
				submit : '".tc('Ok')."',
				style  : 'inherit',
				submitdata : function() {
					return {id : id_elem};
				}
			});
		}
	",
	CClientScript::POS_HEAD);

$this->widget('CustomGridView', array(
	'id'=>'order-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'afterAjaxUpdate' => 'function(){$("a[rel=\'tooltip\']").tooltip(); $("div.tooltip-arrow").remove(); $("div.tooltip-inner").remove();}',
	'columns'=>array(
		array(
      'class'=>'CCheckBoxColumn',
      'id'=>'itemsSelected',
      'selectableRows' => '2',
      'htmlOptions' => array(
        'class'=>'center',
      ),
    ),
    array(
			'header' => tc('Status'),
      'name' => 'status',
      'type' => 'raw',
		  'value' => 'Yii::app()->controller->statusClickDown("order-grid", $data)',
      'htmlOptions' => array(
        'style' => 'width: 100px;',
      ),
		  'sortable' => TRUE,
		  'filter' => Order::statusStateArray(),
		),
		array(
			'name' => 'lead',
      'header' => tc('Lead'),
			'value' => '$data->lead->name',
		  'sortable' => TRUE,
		),
    array(
		  'header' => tc('Type'),
  		'type' => 'raw',
	  	'value' => 'Apartment::getNameByType($data->apartment->type)',
		  'filter' => CHtml::dropDownList('apartment_type', 0, Apartment::getTypesArray(true)),
		  'sortable' => TRUE,
  	),
    array(
			'header' => tc('Address'),
      'value' => '$data->apartment->address',
		  'sortable' => TRUE,
		),
    array(
		  'header' => tc('District'),
  		'value' => '$data->apartment->district_id ? $data->apartment->district->name : ""',
	  	'sortable' => TRUE,
		  'filter' => CHtml::dropDownList('apartment_district', 0, ApartmentDistrict::model()->getListByCity(Yii::app()->params['defaultCityId'])),
  	),
    array(
			'header' => tc('Rooms'),
      'value' => '$data->apartment->num_of_rooms',
		  'sortable' => TRUE,
      'filter' => CHtml::dropDownList('apartment_rooms', 0, Apartment::model()->listValues('num_of_rooms', TRUE)),
		),
    array(
			'header' => tc('Floor'),
      'value' => '$data->apartment->floor',
		  'sortable' => TRUE,
      'filter' => CHtml::dropDownList('apartment_floors', 0, Apartment::model()->listValues('floor', TRUE)),
		),
    array(
			'header' => tc('Price'),
      'value' => '$data->apartment->price',
		  'sortable' => TRUE,
      'filter' => CHtml::dropDownList('apartment_price', 0, Apartment::model()->listValues('price', TRUE)),
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update}{delete}',
			'deleteConfirmation' => tc('Are you sure you want to delete this item?'),
			'htmlOptions' => array('class'=>'infopages_buttons_column'),
      'afterDelete'=>'function(link,success,data){ if(success) $("#statusMsg").html(data); }',
		),
	),
));

$this->renderPartial('//site/admin-select-items', array(
	'url' => '/orders/backend/main/itemsSelected',
	'id' => 'order-grid',
	'model' => $model,
	'options' => array(
		'delete' => Yii::t('common', 'Delete')
	),
));
