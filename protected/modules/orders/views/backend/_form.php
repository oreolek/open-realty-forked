<div class="form">

<?php $form=$this->beginWidget('CustomForm', array(
	'id'=>$this->modelName.'-form',
	'enableAjaxValidation'=>true,
)); ?>
<fieldset>

	<p class="note"><?php echo Yii::t('common', 'Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary($model); ?>

<div class="rowold">
    <?php echo $form->labelEx($model, 'status'); ?>
    <?php echo $form->dropDownList($model, 'status', array(
      Order::STATUS_INACTIVE => tc('Inactive'),
      Order::STATUS_ACTIVE => tc('Active'),
      Order::STATUS_DONE => tc('Done'),
    )); ?>
    <?php echo $form->error($model, 'status'); ?>
</div>

  <div class="rowold">
    <div class="span6">
      <?php echo $form->labelEx($model, 'lead_id'); ?>
      <?php echo $form->dropDownList($model, 'lead_id', Lead::model()->getList(), array('id' => 'leadSelect')); ?>
      <?php echo $form->error($model, 'lead_id'); ?>
      <a href="<?php echo Yii::app()->createUrl('leads/backend/main/update').'?id='.Lead::model()->getLowestId() ?>" class="editlink"><?php echo tt('Edit lead') ?></a>
      <a href="<?php echo Yii::app()->createUrl('leads/backend/main/create') ?>"><?php echo tt('Add lead') ?></a>
    </div>
    <div class="span6">
    <?php if (Yii::app()->user->getState('isAdmin')) { ?>
      <?php echo $form->labelEx($model, 'user_id'); ?>
      <?php $users = CHtml::listData(User::model()->findAll(), 'id', 'username'); ?>
      <?php echo $form->dropDownList($model, 'user_id', $users); ?>
      <?php echo $form->error($model, 'user_id'); ?>
    <?php } ?>
    </div>
  </div>

<!-- if manager or greater -->


  <div class="rowold">
      <div class="span6">
        <label><?php echo tt('City'); ?></label>
        <?php echo CHtml::dropDownList('city_id', Yii::app()->params['defaultCityId'], ApartmentCity::model()->getList(), array('class' => 'width285 searchField')); ?>
      </div>
      <div class="span6">
        <?php echo $form->labelEx($model, 'district_id'); ?>
        <?php echo $form->dropDownList($model,'district_id',ApartmentDistrict::model()->getListByCity(Yii::app()->params['defaultCityId']),
          array('id'=>'ap_region',
            'ajax' => array(
              'type'=>'GET', //request type
              'url'=>$this->createUrl('/api/districts'), //url to call.
              'data'=>'js:"id="+$("#city_id").val()',
              'success'=>'function(result){
								$("#ap_city").html(result);
  						}'
            ),
            'class' => 'width240'
          )
        ); ?>
        <?php echo $form->error($model, 'district_id'); ?>
      </div>
    </div>

<div class="clear"></div>
<div class="rowold">
  <?php echo $form->label($model, 'price', array('required' => true, 'class' => 'control-label'));  ?>

  <div id="price_fields">
    <div class="span4">
      <?php echo tc('price_min') ?>
      <?php echo $form->textField($model, 'price_min', array('class' => 'width100 noblock')); ?>
    </div>
    <div class="span4">
      <?php echo tc('price_max') ?>
      <?php echo $form->textField($model, 'price_max', array('class' => 'width100')); ?>
    </div>

<?php  /*if($model->type == Apartment::TYPE_RENT){
    $priceArray = Apartment::getPriceArray($model->type);
    if(!in_array($model->price_type, array_keys($priceArray))){
      $model->price_type = Apartment::PRICE_PER_MONTH;
    }
    echo '&nbsp;'.$form->dropDownList($model, 'price_type', Apartment::getPriceArray($model->type), array('class' => 'width150'));
  }*/ ?>
  </div>

    <?php echo $form->error($model, 'price'); ?>
</div>
  <div class="clear"></div>

  <div class="rowold">
    <?php echo $form->labelEx($model, 'square'); ?>
    <?php echo Apartment::getTip('square');?>
    <?php echo $form->error($model, 'square'); ?>
    <div class="span4">
      <?php echo tc('square_min') ?>
      <?php echo $form->textField($model, 'square_min', array('size' => 5, 'class' => 'width100 noblock')).' '.tc('site_square'); ?>
    </div>
    <div class="span4">
      <?php echo tc('square_max') ?>
      <?php echo $form->textField($model, 'square_max', array('size' => 5, 'class' => 'width100')).' '.tc('site_square'); ?>
    </div>
  </div>
  <div class="clear"></div>

<div class="rowold">
<?php
echo $form->labelEx($model, 'floor');
echo $form->textField($model, 'floor', array('class' => 'form-control'));
echo $form->error($model, 'florr');
?>
</div>

<div class="rowold">
<?php echo $form->labelEx($model, 'rooms'); ?>
<?php echo $form->error($model, 'rooms'); ?>
<div class="span4">
      <?php echo tc('rooms_min') ?>
      <?php echo $form->textField($model, 'rooms_min', array('size' => 5, 'class' => 'width100 noblock')).' '.tc('site_square'); ?>
    </div>
    <div class="span4">
      <?php echo tc('rooms_max') ?>
      <?php echo $form->textField($model, 'rooms_max', array('size' => 5, 'class' => 'width100')).' '.tc('site_square'); ?>
    </div>
</div>

<div class="clear"></div>

<div class="rowold">
  <?php echo $form->labelEx($model, 'apartment_id'); ?>
  <div id="apartment_dropdown">
    <?php echo $form->dropDownList($model, 'apartment_id', Apartment::model()->listFree()); ?>
  </div>
  <?php echo $form->error($model, 'apartment_id'); ?>
  <a href="<?php echo Yii::app()->createUrl('apartments/backend/main/create') ?>"><?php echo tt('Add apartment') ?></a>
</div>

<div class="rowold">
<?php
echo $form->labelEx($model, 'additional');
echo $form->textArea($model, 'additional', array('class' => 'form-control span12'));
echo $form->error($model, 'additional');
?>
</div>

	<div class="clear"></div>

    <div class="rowold buttons">
           <?php $this->widget('bootstrap.widgets.TbButton',
                       array('buttonType'=>'submit',
                           'type'=>'primary',
                           'icon'=>'ok white',
                           'label'=> $model->isNewRecord ? tc('Add') : tc('Save'),
                       )); ?>
   	</div>

</fieldset>
<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
$(document).ready(function(){
  $("input").on("change", function(){
    if ($(this).attr("name") == "apartment_id")
    {
      return TRUE;
    }
    $.get("<?php echo Yii::app()->createUrl('/orders/backend/main/searchApartments') ?>", $("#<?php echo $this->modelName ?>-form").serialize(), function(data){
      $("#apartment_dropdown").html(data);
    });
  });
  $("#leadSelect").on("change", function(){
    $(this).closest(".editLink").attr("href", "<?php echo Yii::app()->createUrl('leads/backend/main/edit/') ?>"+$(this).val());
  });
});
</script>
