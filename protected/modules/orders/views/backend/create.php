<?php
$this->breadcrumbs=array(
	tt('Manage orders') => array('admin'),
	tt('Add order'),
);

$this->menu=array(
	array('label'=>tt('Manage orders'), 'url'=>array('admin')),
);

$this->adminTitle = tt('Add order');
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
