<?php
/**********************************************************************************************
* @author Alexander Yakovlev
* This work is licensed under a GNU GPL.
* http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
***********************************************************************************************/

class MainController extends ModuleAdminController{
	public $modelName = 'Order';
  public $allowAll = TRUE;

	public function actionView($id){
		$this->redirect(array('admin'));
	}
	public function actionIndex(){
		$this->redirect(array('admin'));
	}
	public function actionActivate(){
    $id = Yii::app()->request->getParam('id');
    $value = Yii::app()->request->getParam('value');
    $order = Order::model()->findByPK($id);
    $order->status = $value;
    $order->save();
    echo $this->statusClickDown('order-grid', $order);
  }

	public function actionAdmin(){
		$this->getMaxSorter();
    $cs = Yii::app()->clientScript;
    $cs->registerCoreScript('jquery.ui');
    $cs->registerScriptFile($cs->getCoreScriptUrl(). '/jui/js/jquery-ui-i18n.min.js');
    $cs->registerCssFile($cs->getCoreScriptUrl(). '/jui/css/base/jquery-ui.css');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.jeditable.js', CClientScript::POS_END);
    $this->breadcrumbs=array(
    	tt('Manage orders')
    );
    $this->adminTitle = tt('Manage orders');
    $this->menu=array(
    	array('label'=>tt('Add order'), 'url'=>array('/orders/backend/main/create')),
    );
		parent::actionAdmin();
	}

  public function statusDropDown($model = NULL)
  {
    $status = 0;
    if ($model)
    {
      $status = $model->status;
    }
    return CHtml::dropDownList('status', $status, Order::statusStateArray(), array('id' => uniqid(), 'class' => 'select-order'));
  }

  public function statusClickDown($tableId, $data)
  {
    $moderationStates = Order::statusStateArray();
		$items = CJavaScript::encode($moderationStates);
		$options = array(
			'onclick' => 'ajaxSetModerationStatus(this, "'.$tableId.'", "'.$data->id.'", "'.$data->user_id.'", "'.$items.'"); return false;',
		);

		return '<div align="center" class="editable_select" id="editable_select-'.$data->id.'">'.CHtml::link($moderationStates[$data->status], '#' , $options).'</div>';
  }

  public function actionSearchApartments()
  {
    $model = new Apartment;
    $model->attributes = Yii::app()->request->getParam($this->modelName);
    $model->isFree = TRUE;
    $data = $model->search()->getData();
    if (empty($data))
    {
      echo 'Подходящих объектов не найдено.';
    }
    else
    {
      echo CHtml::activeDropDownList(Order::model(), 'apartment_id', CHtml::listData($data, 'id', 'name_ru'));
    }
  }
}
