<?php
$this->breadcrumbs=array(
	//Yii::t('common', 'Object type') => array('/site/viewreferences'),
	tt('Manage apartment district')=>array('admin'),
	tt('Add city'),
);

$this->menu=array(
	array('label'=>tt('Manage apartment district'), 'url'=>array('admin')),
	//array('label'=>tt('Add city'), 'url'=>array('/apartmentCity/backend/main/create')),
);

$this->adminTitle = tt('Add district');
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
