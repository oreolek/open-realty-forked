<?php
$this->breadcrumbs=array(
	//Yii::t('common', 'References') => array('/site/viewreferences'),
	tt('Manage apartment district')=>array('admin'),
	tt('Edit city'),
);

$this->menu=array(
    array('label'=>tt('Manage apartment district'), 'url'=>array('admin')),
    array('label'=>tt('Add district'), 'url'=>array('/apartmentCity/backend/main/create')),
);

$this->adminTitle = tt('Edit district');
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
