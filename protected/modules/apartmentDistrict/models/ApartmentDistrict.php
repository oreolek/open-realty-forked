<?php
/**
 * @author Alexander Yakovlev
 * This work is licensed under a GNU GPL.
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * This is the model class for table "{{districts}}".
 *
 * The followings are the available columns in table '{{districts}}':
 * @property integer $id
 * @property integer $city_id
 * @property string $name
 */
class ApartmentDistrict extends ApartmentDistrictBase
{
  /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return District the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

  public static function getListByCity($id)
  {
    if ($id == 0)
    {
      $id = Yii::app()->params['defaultCityId'];
    }
    $districts = self::model()->findAll('city_id = '.$id);
    return CHtml::listData($districts, 'id', self::model()->listField);
  }
}
