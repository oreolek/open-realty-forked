<?php
/**
 * @author Alexander Yakovlev
 * This work is licensed under a GNU GPL.
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * This is the model class for table "{{districts}}".
 *
 * The followings are the available columns in table '{{districts}}':
 * @property integer $id
 * @property integer $city_id
 * @property string $name
 */
class ApartmentDistrictBase extends ParentModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{districts}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('city_id, name_ru', 'required'),
			array('city_id', 'numerical', 'integerOnly'=>true),
			array('name_ru', 'length', 'max'=>255),
			array('city_id, name_ru', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'city' => array(self::BELONGS_TO, 'ApartmentCity', 'city_id'),
		);
	}

  public function beforeSave(){
		if($this->isNewRecord){
			$maxSorter = Yii::app()->db->createCommand()
				->select('MAX(sorter) as maxSorter')
				->from($this->tableName())
				->queryScalar();
			$this->sorter = $maxSorter+1;
		}

		return parent::beforeSave();
	}

  public function i18nFields(){
		return array(
			'name' => 'varchar(255) not null',
		);
	}

  public function getName(){
		return $this->getStrByLang('name');
	}

  public function beforeDelete(){
		if($this->model()->count() <= 1){
			return false;
		}

		$sql = "UPDATE {{apartment}} SET district_id=0, active=0 WHERE district_id=".$this->id;
		Yii::app()->db->createCommand($sql)->execute();

		return parent::beforeDelete();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'city_id' => Yii::t('front', 'City'),
			'name_ru' => Yii::t('front', 'Name'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('name_ru',$this->name_ru,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
      'pagination'=>array(
				'pageSize'=>param('adminPaginationPageSize', 20),
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return District the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
